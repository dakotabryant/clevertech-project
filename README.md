## Problem statement

Complete the functions in `mongo-commands.js` in order to create a map reduce routine that reports each student’s primary key, their name, and the number of courses they are enrolled in. 
Store the output of this routine in the collection `coursereport`. 

Once ```mongo-commands.js``` is executed the collection ```coursereport``` should contain the following information:

```
[
{ "_id" : "jeff", "value" : { "name" : "Jeff Holland", "numbercourses" : 2 } },
{ "_id" : "john.shore", "value" : { "name" : "John Shore", "numbercourses" : 2 } },
{ "_id" : "lee2331", "value" : { "name" : "Lee Aldwell", "numbercourses" : 1 } },
{ "_id" : "rcotter", "value" : { "name" : "Ray Cotter", "numbercourses" : 2 } },
{ "_id" : "scott", "value" : { "name" : "Scott Mills", "numbercourses" : 3 } } 
]
```
