const dataImport = require("./data-import");

//produce a listing of student full name and how many courses he/she is taking
async function produceReport(db, callback) {
  const students = await db
    .collection("students")
    .find()
    .toArray();
  const courses = await db
    .collection("courses")
    .find()
    .toArray();
  const coursereport = await db.createCollection("coursereport");
  const report = students.map(({ _id, name }) => {
    let numbercourses = 0;
    const currentCourse = courses.forEach(course => {
      if (course.students.find(element => element === _id)) numbercourses++;
      return;
    });
    return {
      _id,
      value: {
        name: `${name.first} ${name.last}`,
        numbercourses
      }
    };
  });

  coursereport.insertMany(report);

  // After creating the listing into coursereport collection, call callback() function
  callback();
}

module.exports = produceReport;
