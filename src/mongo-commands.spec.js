var assert = require("assert");
var MongoClient = require("mongodb").MongoClient;
var fs = require("fs");

describe("mongo-commands", function() {
  var url = "mongodb://127.0.0.1:27017/myproject";

  var db;
  describe("coursereport", function() {
    it.only(
      "should contain 5 records",
      executeOnDb(function(report, done) {
        report.find({}).toArray(function(err, docs) {
          assert.equal(err, null);
          assert.equal(docs.length, 5);
          done();
        });
      })
    );
    it(
      "should contain correct course number for Jeff",
      executeOnDb(function(report, done) {
        report.findOne({ _id: "jeff" }, function(err, doc) {
          assert.equal(err, null);
          assert.equal(doc.value.numbercourses, 2);
          done();
        });
      })
    );
    it(
      "should contain correct name for Jeff",
      executeOnDb(function(report, done) {
        report.findOne({ _id: "jeff" }, function(err, doc) {
          assert.equal(err, null);
          assert.equal(doc.value.name, "Jeff Holland");
          done();
        });
      })
    );
    it(
      "should contain correct course number for john.shore",
      executeOnDb(function(report, done) {
        report.findOne({ _id: "john.shore" }, function(err, doc) {
          assert.equal(err, null);
          assert.equal(doc.value.numbercourses, 2);
          done();
        });
      })
    );
    it(
      "should contain correct name for john.shore",
      executeOnDb(function(report, done) {
        report.findOne({ _id: "john.shore" }, function(err, doc) {
          assert.equal(err, null);
          assert.equal(doc.value.name, "John Shore");
          done();
        });
      })
    );
    it(
      "should contain correct course number for scott",
      executeOnDb(function(report, done) {
        report.findOne({ _id: "scott" }, function(err, doc) {
          assert.equal(err, null);
          assert.equal(doc.value.numbercourses, 3);
          done();
        });
      })
    );
    it(
      "should contain correct name for scott",
      executeOnDb(function(report, done) {
        report.findOne({ _id: "scott" }, function(err, doc) {
          assert.equal(err, null);
          assert.equal(doc.value.name, "Scott Mills");
          done();
        });
      })
    );
  });

  function executeOnDb(assertions) {
    return function(done) {
      var script = require("./mongo-commands");
      script(db, function() {
        var report = db.collection("coursereport");
        assertions(report, done);
      });
    };
  }

  beforeEach(function(done) {
    MongoClient.connect(
      url,
      function(err, connectedDb) {
        if (err) {
          return done(err);
        }
        db = connectedDb;
        db.dropDatabase();
        var dataImport = require("./data-import");
        dataImport(db, function() {
          done();
        });
      }
    );
  });

  afterEach(function() {
    db.close();
  });
});
