'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var mocha = require('gulp-mocha');

function handleError(err) {
  console.log(err.toString());
  this.emit('end');
}

gulp.task('test', function (done) {
  console.log("Starting test");

  var mocha = require('gulp-mocha');
  gulp.src('src/{,**/}*.spec.js', {read: false})
    .pipe(mocha({
      reporter: 'mocha-jenkins-reporter',
      timeout : 5000,
      reporterOptions: {
        "junit_report_path": "test-results.xml"
      }
    })
      .on("error", handleError));

  done();
});
